package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"time"
)

func version() {
	os.Stderr.WriteString(`teesay: v0.3.0
`)
}

func usage() {
	os.Stderr.WriteString(`
Usage: teesay [OPTION] [FILE]...
Let bouyomichan read text in FILE(s) or standard input.

Options:
	--port  -p    port number of host
	--ip    -i    IP address of host
	-sjis         read Shift-JIS text
	-utf8         read UTF-8 text
	-utf16        read UTF-16 text

	--help        show this help message
	--version     print the version

Report bugs to <kusabashira227@gmail.com>
`[1:])
}

var correspondEncoding = map[string]int8{
	"utf8":  0,
	"utf16": 1,
	"sjis":  2,
}

type BouyomiWriter struct {
	command  int16
	speed    int16
	volume   int16
	interval int16
	category int16
	encode   int8
	connect  net.Conn
}

func NewBouyoumiWriter(conn net.Conn, encoding string) *BouyomiWriter {
	return &BouyomiWriter{
		command:  0x0001,
		speed:    -1,
		volume:   -1,
		interval: -1,
		category: 0,
		encode:   correspondEncoding[encoding],
		connect:  conn,
	}
}

func (y *BouyomiWriter) createBody(src []byte) []byte {
	buf := bytes.NewBuffer(make([]byte, 0))
	binary.Write(buf, binary.LittleEndian, y.command)
	binary.Write(buf, binary.LittleEndian, y.speed)
	binary.Write(buf, binary.LittleEndian, y.volume)
	binary.Write(buf, binary.LittleEndian, y.interval)
	binary.Write(buf, binary.LittleEndian, y.category)
	binary.Write(buf, binary.LittleEndian, y.encode)
	binary.Write(buf, binary.LittleEndian, int32(len(src)))
	binary.Write(buf, binary.LittleEndian, src)
	return buf.Bytes()
}

func (y *BouyomiWriter) Write(p []byte) (n int, err error) {
	body := y.createBody(p)
	_, err = y.connect.Write(body)
	return len(body), err
}

func _main() (exitCode int, err error) {
	var portNum int
	var hostIP string
	var isUTF8, isUTF16, isSJIS bool
	flag.IntVar(&portNum, "p", 50001, "port number of host")
	flag.IntVar(&portNum, "port", 50001, "port number of host")
	flag.StringVar(&hostIP, "i", "localhost", "IP address of host")
	flag.StringVar(&hostIP, "ip", "localhost", "IP address of host")
	flag.BoolVar(&isUTF8, "utf8", false, "read UTF-8 text")
	flag.BoolVar(&isUTF16, "utf16", false, "read UTF-16 text")
	flag.BoolVar(&isSJIS, "sjis", false, "read ShiftJIS text")

	var isHelp, isVersion bool
	flag.BoolVar(&isHelp, "help", false, "show this help message")
	flag.BoolVar(&isVersion, "version", false, "print the version")
	flag.Usage = usage
	flag.Parse()

	if isHelp {
		usage()
		return 0, nil
	}
	if isVersion {
		version()
		return 0, nil
	}

	var encoding string
	if (isUTF8 && isUTF16) || (isUTF16 && isSJIS) || (isSJIS && isUTF8) {
		return 1, fmt.Errorf("cannot specify multi encoding")
	}
	switch {
	case isUTF8:
		encoding = "utf8"
	case isUTF16:
		encoding = "utf16"
	case isSJIS:
		encoding = "sjis"
	default:
		encoding = "utf8"
	}

	var input io.Reader
	if flag.NArg() < 1 {
		input = os.Stdin
	} else {
		var inputs []io.Reader
		for _, fname := range flag.Args() {
			in, err := os.Open(fname)
			if err != nil {
				return 1, err
			}
			defer in.Close()
			inputs = append(inputs, in)
		}
		input = io.MultiReader(inputs...)
	}

	address := fmt.Sprintf("%s:%d", hostIP, portNum)
	con, err := net.DialTimeout("tcp", address, 5*time.Second)
	if err != nil {
		return 1, err
	}
	defer con.Close()

	bouyoumi := NewBouyoumiWriter(con, encoding)
	_, err = io.Copy(os.Stdout, io.TeeReader(input, bouyoumi))
	if err != nil {
		return 1, err
	}

	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "teesay:", err)
	}
	os.Exit(exitCode)
}
